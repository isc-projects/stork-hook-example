# Stork example hook

It is the most straightforward hook for Stork Server. It shows how the Stork
hook may be written.

## How to build

Use the `rake build` command to build the hook file. It will be located in the
`build` directory. 

## How to use

Put the `*.so` file in the Stork Server hook directory. After that, you can log
in to the application using login `secret` and password `secret`.

## Troubleshooting

The Stork hook framework is under development yet. The hook may be unstable,
not working, or its API may be changed. Use for experimental purposes only.
