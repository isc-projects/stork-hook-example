package main

import (
	"embed"
	"io"
)

//go:embed icon.png
var icon embed.FS

type authenticationMetadata struct{}

func (m *authenticationMetadata) GetID() string { return "example" }

func (m *authenticationMetadata) GetName() string { return "Example hook" }

func (m *authenticationMetadata) GetDescription() string {
	return "It is an example Stork authentication hook. Use the login " +
		"with 'super' keyword to log-in as super-admin, with 'admin' keyword " +
		"to log-in as admin. Otherwise the user groups will managed by Stork " +
		"itself. The accepted password is 'secret'."
}

func (m *authenticationMetadata) GetIcon() (io.ReadCloser, error) {
	return icon.Open("icon.png")
}

func (m *authenticationMetadata) GetIdentifierFormLabel() string { return "Login" }

func (m *authenticationMetadata) GetSecretFormLabel() string { return "Password" }
