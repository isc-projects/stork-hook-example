package main

import (
	"isc.org/stork/hooks"
)

func Load(hooks.HookSettings) (hooks.CalloutCarrier, error) {
	return &callouts{}, nil
}

func GetVersion() (string, string) {
	return hooks.HookProgramServer, hooks.StorkVersion
}

// Type guards.
var (
	_ hooks.HookLoadFunction       = Load
	_ hooks.HookGetVersionFunction = GetVersion
)
