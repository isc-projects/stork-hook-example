module isc.org/stork-hook-example

go 1.18

replace isc.org/stork => gitlab.isc.org/isc-projects/stork/backend v1.12.1-0.20230912092650-e5c352a0b197

require isc.org/stork v0.0.0
