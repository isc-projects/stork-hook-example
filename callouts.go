package main

import (
	"context"
	"errors"
	"fmt"
	"hash/fnv"
	"math/rand"
	"net/http"
	"strings"

	"isc.org/stork/hooks/server/authenticationcallouts"
)

type callouts struct{}

var _ authenticationcallouts.AuthenticationCallouts = (*callouts)(nil)

func (c *callouts) Close() error {
	return nil
}

func (c *callouts) Authenticate(ctx context.Context, request *http.Request, email, password *string) (*authenticationcallouts.User, error) {
	if email == nil || password == nil {
		return nil, errors.New("missing email or password")
	}

	if *password != "secret" {
		return nil, errors.New("invalid user or password")
	}

	var groups []authenticationcallouts.UserGroupID

	switch {
	case strings.Contains(strings.ToLower(*email), "super"):
		groups = append(groups, authenticationcallouts.UserGroupIDSuperAdmin)
	case strings.Contains(strings.ToLower(*email), "admin"):
		groups = append(groups, authenticationcallouts.UserGroupIDAdmin)
	}

	if !strings.Contains(*email, "@") {
		*email += "@example.com"
	}

	atIndex := strings.Index(*email, "@")
	login := (*email)[0:atIndex]

	hash := fnv.New32a()
	hash.Write([]byte(*email))
	id := hash.Sum32()

	names := []string{"Alice", "Bob", "Clive", "Denis", "Elise", "Felix",
		"George", "Harry", "Irene", "Jekyll", "Kate", "Lisa", "Morris", "Naomi",
		"Oliver", "Peter", "Quentin", "Randal", "Steve", "Ted", "Uve", "Vivian",
		"Willy", "Xen", "Yuan", "Zoe"}

	return &authenticationcallouts.User{
		ID:       fmt.Sprint(id),
		Login:    login,
		Email:    *email,
		Lastname: names[rand.Intn(len(names))],
		Name:     names[rand.Intn(len(names))],
		Groups:   groups,
	}, nil
}

func (c *callouts) Unauthenticate(ctx context.Context) error {
	return nil
}

func (c *callouts) GetMetadata() authenticationcallouts.AuthenticationMetadata {
	return &authenticationMetadata{}
}
